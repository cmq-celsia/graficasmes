<em>(This chart is a part of the `d3-charts` collection available [here][collection].)</em>

* * * *

This chart displays the share of votes over time between two Danish parliamentary coalitions using placeholder data. The chart here has been used for two coalitions, but can also be used for two candidates.

You will notice something enveloping the line charts, which is the *[confidence interval][ci]*, which indicates the spectrum of the confidence in our displayed results---to use a completely unscientific definition.

Compare this to [Sam Wang's chart for the U.S. 2012 election][wang] and [Drew Linzer's charts for individual states][votamatic].

Usage
-----
All parameters are in the top of the script under `// Config`. If we use the example script:

```js
var dataset = "data.csv",
    parseDate = d3.time.format("%Y/%m").parse,
    electionDate = "",  // "2014/11",
    interpolation = "linear";

var coalitionLeft = ["A", "B", "F", "Ø"],
    coalitionLeftColor = "#D7191C", // blue
    coalitionRight = ["V", "O", "K", "I", "C"],
    coalitionRightColor = "#2B83BA", // red
    displaySingleCoalition = false;
    // false, "left", "right"

var useCoalitionLabels = true,
    yAxisTitle = "Votes (%)",
    cutoff = 50;

if (useCoalitionLabels === true) { margin.right = 50; }
```

* `parseDate` refers to the date format---in this case `YYYY/MM`.
* `electionDate` refers to the date of the election, if known. This will change the maximum on the x-axis to said date to contextualize the trends.
* `interpolation` refers to the "smoothing" of the graphs. More info [here][interpolation].
* `coalitionLeft/coalitionRight` refer to the letters of each member party of a coalition, entered as strings in an array.
* `useCoalitionLabels` refers to whether the developer wishes to display a coalition name (made by concatenating all its parties’ letters) at the end of the respective graph.
* `displaySingleCoalition` refers to the fact that elections are often a zero-sum game. This option allows you to display just one of the two coalitions.
* `cutoff` refers to the % at which the dotted divider should be set. In some cases, 50% may not be the desired value.

The other parameters should be self-explanatory.

The data should have a column with (1) a date, one for (2) each party or candidate, and one for (3) the number of respondents.

Future Features to Consider
---------------------------
* Easier input for choosing between competing coalitions and candidates.

Issues
------
* [Subticks don't show][issue-subticks].
* No support for asymmetric confidence interval ([GitHub issue link][issue-asymmetry]).


[collection]: https://github.com/ndarville/d3-charts/
[ci]: https://en.wikipedia.org/wiki/Confidence_interval
[votamatic]: http://votamatic.org/forecast-tracker/
[wang]: http://election.princeton.edu/history-of-electoral-votes-for-obama
[interpolation]: http://www.d3noob.org/2013/01/smoothing-out-lines-in-d3js.html
[issue-asymmetry]: https://github.com/ndarville/d3-charts/issues/3
[issue-subticks]: https://github.com/ndarville/d3-charts/issues/1
